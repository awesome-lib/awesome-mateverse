# Mateverse（元宇宙）资源大全

有关Mateverse的精选大全，包含元宇宙起源、相关产品、技术等介绍说明

### 什么是“元宇宙”？

元宇宙（awesome-mateverse），是一个既可以映射现实世界，又可以独立于现实世界的虚拟空间。游戏平台Roblox上市后的市值狂飙，带火了“元宇宙”的概念。目前，按照Roblox的官方说法，元宇宙产品应具备八大要素：身份、朋友、沉浸感、低延迟、多元化、随地、经济系统、文明。

[百度百科：《元宇宙》](https://baike.baidu.com/item/元宇宙/58292530?fr=aladdin)

### 目录

元宇宙的起源

元宇宙的底层技术

- 六大技术体系

元宇宙世界的构建

- 开源文化与元宇宙
- 建筑设计在Metaverse中的作用
- 从NFT艺术，看元宇宙的经济系统

------

### 元宇宙的起源

#### “元宇宙”的初次出现

1992年，美国著名科幻作家尼尔·斯蒂芬森（Neal Stephenson）推出了自己的小说《雪崩（Snow Crash）》。在书中，尼尔·斯蒂芬森描述了一个平行于现实世界的网络世界，所有现实世界中的人，在元界中都有一个“网络分身”。

尼尔·斯蒂芬森将这个网络世界，命名为“元界”，英文原著中叫“Metaverse”。它由Meta和Verse两个词根组成，Meta表示“超越”、“元”， verse表示“宇宙universe”。这便是元宇宙的诞生。

#### Roblox如何借“元宇宙”大火

[《暴涨十倍，Roblox靠小学生氪金创造了市值奇迹》](https://36kr.com/p/1133065727493255)——

### 元宇宙的底层技术

![输入图片说明](https://images.gitee.com/uploads/images/2021/0915/194431_cbf951c7_9459537.png "屏幕截图.png")
元宇宙六大支撑技术

图片来源：《元宇宙通证》

### 元宇宙世界的构建

#### 开源文化与元宇宙

#### 建筑设计在Metaverse中的作用

[《元宇宙的“造梦师”：建筑设计在 Metaverse 中的作用》](https://mp.weixin.qq.com/s?__biz=MzkwODI2MzY4Ng==&mid=2247483657&idx=1&sn=cac8064031535d0e66bdd35bb2bbef79&chksm=c0cde960f7ba60764e12eca0777cc327ff2c6d7fdf7e5d6172c0b39587f78086c11172c87bd4&scene=178&cur_album_id=2012414442180788229#rd)——3D可视化技术与Metaverse的互动发展，让这样的建筑技术可以在虚拟空间中成为可能。

[《NVIDIA扩展Omniverse平台，将数百万新用户带入元宇宙的世界》](https://mp.weixin.qq.com/s/6WjHhYnyneEAZRlWsGQrDQ)——NVIDIA于太平洋时间2021年8月10日宣布，全球首个为元宇宙建立提供基础的模拟和协作平台——NVIDIA Omniverse™将通过与Blender和Adobe集成来实现大规模扩展，并将向数百万新用户开放。

#### 从NFT艺术，看元宇宙的经济系统

[《元宇宙的艺术生成:追溯NFT艺术的源头》](https://kns.cnki.net/kcms/detail/detail.aspx?dbcode=CJFD&dbname=CJFDAUTO&filename=CHMS202104007&uniplatform=NZKPT&v=0eGyKfKi6J8R1F%25mmd2BpUWPBjRsAI4dSCrE%25mmd2Bz8%25mmd2BW2rx4m42Jr16fxzAOF7ejmfCmgh1v)——
NFT艺术与近十年来由科幻文本进入现实建构的"元宇宙"概念形成了富有深意的互文关系。通过对数字身份、公平交易和游戏性的讨论,来探讨NFT这一数字机制的生成与流变。 